﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InternalSorts
{
    public partial class FormAdd : Form
    {
        public List<Country> countries1 { get;set; }
        public FormAdd(List<Country> countries)
        {
            InitializeComponent();
            countries1 = countries;
        }

        private void Add_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            Country country = new Country();
            country.Name = textBox2.Text;
            country.Area = Int32.Parse( textBox6.Text);
            country.Capital = textBox4.Text;
            country.Continent = textBox3.Text;
            country.PoliticalSystem = textBox5.Text;
            country.Population = Int32.Parse(textBox7.Text);
            countries1.Add(country);
        }
    }
}
