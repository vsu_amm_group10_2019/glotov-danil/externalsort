﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*В ООН имеется полный перечень всех стран, который включает в себя:
 название;
 континент;
 столицу;
 площадь;
 численность населения;
 государственный строй.*/
namespace InternalSorts
{
    public class Country
    {
        public string Name { get; set; }
        public string Continent { get; set; }
        public string Capital { get; set; }
        public string PoliticalSystem { get; set; }
        public int Area { get; set; }
        public int Population { get; set; }

        public bool HasElement { get; set; }

        public Country()
        {
            Name = "";
            Continent = "";
            Capital = "";
            PoliticalSystem = "";
            
        }
    }
}
